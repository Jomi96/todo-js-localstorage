document.addEventListener("DOMContentLoaded", function () {
    loadTasks();
  });
  
  function addTask() {
    const taskInput = document.getElementById("taskInput");
    const taskText = taskInput.value.trim();
    const taskDateInput = document.getElementById("taskDate");
    const taskDate = taskDateInput.value;
  
    if (taskText === "" || taskDate === "") return;
  
    const tasks = getTasksFromLocalStorage();
    tasks.push({ text: taskText, done: false, date: taskDate });
  
    localStorage.setItem("tasks", JSON.stringify(tasks));
    taskInput.value = "";
    taskDateInput.value = "";
  
    loadTasks();
  }
  
  function editTask(index) {
    const tasks = getTasksFromLocalStorage();
    const newText = prompt("Edit task:", tasks[index].text);
  
    if (newText !== null) {
      tasks[index].text = newText.trim();
      localStorage.setItem("tasks", JSON.stringify(tasks));
      loadTasks();
    }
  }
  
  function deleteTask(index) {
    const confirmDelete = confirm("Are you sure you want to delete this task?");
  
    if (confirmDelete) {
      const tasks = getTasksFromLocalStorage();
      tasks.splice(index, 1);
      localStorage.setItem("tasks", JSON.stringify(tasks));
      loadTasks();
    }
  }
  
  function toggleTask(index) {
    const tasks = getTasksFromLocalStorage();
    tasks[index].done = !tasks[index].done;
    localStorage.setItem("tasks", JSON.stringify(tasks));
    loadTasks();
  }
  
  function loadTasks() {
    const tasks = getTasksFromLocalStorage();
    const taskList = document.getElementById("taskList");
    const paginationContainer = document.getElementById("pagination");
    const itemsPerPage = 5;
  
    // Clear the existing task list
    taskList.innerHTML = "";
  
    // Display tasks based on the current page and items per page
    const currentPage = getCurrentPage();
    const startIndex = (currentPage - 1) * itemsPerPage;
    const endIndex = startIndex + itemsPerPage;
    const displayedTasks = tasks.slice(startIndex, endIndex);
  
    displayedTasks.forEach((task, index) => {
      const listItem = document.createElement("li");
      listItem.innerHTML = `
        <span>${task.text}</span>
        <span class="date">${task.date}</span>
        <input type="checkbox" ${task.done ? "checked" : ""} onchange="toggleTask(${startIndex + index})">
        <button onclick="editTask(${startIndex + index})">Edit</button>
        <button onclick="deleteTask(${startIndex + index})">Delete</button>
      `;
      taskList.appendChild(listItem);
    });
  
    // Update the pagination UI
    updatePagination(currentPage, Math.ceil(tasks.length / itemsPerPage));
  }
  
  function getCurrentPage() {
    return parseInt(localStorage.getItem("currentPage")) || 1;
  }
  
  function setCurrentPage(page) {
    localStorage.setItem("currentPage", page);
  }
  
  function updatePagination(currentPage, totalPages) {
    const paginationContainer = document.getElementById("pagination");
    paginationContainer.innerHTML = "";
  
    for (let i = 1; i <= totalPages; i++) {
      const button = document.createElement("button");
      button.innerText = i;
      button.onclick = () => {
        setCurrentPage(i);
        loadTasks();
      };
  
      if (i === currentPage) {
        button.classList.add("active");
      }
  
      paginationContainer.appendChild(button);
    }
  }
  
  function getTasksFromLocalStorage() {
    return JSON.parse(localStorage.getItem("tasks")) || [];
  }
  